# DownloadFile

DownloadFile is a module to direct download files or images.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/download_file).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/download_file).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Select the 'Direct Download' format for your file fields.

## Maintainers

- Ayesh Karunaratne - [Ayesh](https://www.drupal.org/u/ayesh)
- Ben Mullins - [bnjmnm](https://www.drupal.org/u/bnjmnm)
- Kristoffer Wiklund - [kristofferwiklund](https://www.drupal.org/u/kristofferwiklund)
- Henrik Danielsson - [TwoD](https://www.drupal.org/u/twod)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
